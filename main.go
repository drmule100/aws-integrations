package main

import (
	"fmt"
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	//"gitlab.com/drmule100/aws-integrations/aws_client"
)

func main() {
	fmt.Println("starting server.....")
	//s3Ctx, err := aws_client.New()
	sess, err := session.NewSession()
	if err != nil {
		log.Println("error while creating new session")
		return
		//return nil, err
	}
	s3Session := s3.New(sess, aws.NewConfig().WithRegion("ap-south-1"))

	//list all buckets
	buckets, err := listAllBuckets(s3Session)
	if err != nil {
		log.Println("error in listing buckets")
	} else {
		fmt.Println("Buckets in s3 :", buckets)
	}

	//
	createdBucket, err := createBucket(s3Session, "dnyaneshwar1111")
	if err != nil {
		log.Println("error in creating buckets")
	} else {
		fmt.Println("bucket is created in s3 :", createdBucket)
	}

	buckets, err = listAllBuckets(s3Session)
	if err != nil {
		log.Println("error in listing buckets")
	} else {
		fmt.Println("Buckets in s3 :", buckets)
	}

}

func listAllBuckets(s3Session *s3.S3) (*s3.ListBucketsOutput, error) {
	buckets, err := s3Session.ListBuckets(&s3.ListBucketsInput{})
	if err != nil {
		log.Println("error while getting bucket list: ", err)
	}
	return buckets, err
}

func createBucket(s3Session *s3.S3, bucketName string) (*s3.CreateBucketOutput, error) {
	creatBucketOut, err := s3Session.CreateBucket(
		&s3.CreateBucketInput{
			Bucket: aws.String(bucketName),
			CreateBucketConfiguration: &s3.CreateBucketConfiguration{
				LocationConstraint: aws.String("ap-south-1"),
			},
		},
	)
	if err != nil {
		log.Println(err)

	}
	return creatBucketOut, err
}
