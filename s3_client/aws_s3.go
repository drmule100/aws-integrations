package s3_client

import (
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type Context struct {
	s3Session *s3.S3
}

func New() (*Context, error) {

	sess, err := session.NewSession()
	if err != nil {
		log.Println("error while creating new session")
		return nil, err
	}
	s3Session := s3.New(sess, aws.NewConfig().WithRegion("us-west-2"))
	return &Context{s3Session: s3Session}, nil

}
